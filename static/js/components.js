// Default view with no route
const main = {
    template: `
        <div class="row centre">
            <router-link to="/load/normal" tag="button">Start Quiz</router-link>
            <router-link to="/load/hard" tag="button">Hard Quiz</router-link>
        </div>
    `
}

// View when loading questions
const load = {
    template: `<p>Loading {{type}}</p>`,
    props: {
        type: String
    },
    mounted: function () {
        let success_function = function(data, status){
            process_questions(data, status);
            router.push("/quiz");
        };

        if(this.type == "hard"){
            console.log("Hard quiz api not ready, loading normal");
            request_questions(success_function);
        }else if(this.type == "normal"){
            request_questions(success_function);
        }
    }
}

// Main view for the active quiz
const quiz = {
    props: ["current_question", "questions", "question_number"],
    template: `<div>
                    <h2>{{current_question.text}}</h2>
                    <div class="row centre">
                        <p class="col" id="correct">{{calc_percentage(current_question)}}% of people got this correct</p>
                    </div>

                    <li v-for='answer in current_question.answers' class="answers">
                        <answer v-bind:answer="answer" v-bind:question="current_question"></answer>
                    </li>
                    <button v-if="show_finish_button" v-on:click="next_question">Next Question</button>
                    <button v-else-if="show_next_button" v-on:click="next_question">Next Question</button>
                    <score v-bind:questions="questions"></score>
                    <button class="small-button" @click="$emit('show_flag')">Flag Question</button>
               </div>
        `,
    methods: {
        calc_percentage: function(question){
            percent = (question.times_correct/question.times_asked) * 100;
            rounded = Math.round(percent * 10) /10;
            if (isNaN(rounded)){
                return "0"
            }
            return "" + rounded
        },
        next_question: function(){
            next_question();
            let buttons = this.$children;
            console.log("Buttons: ", buttons);
            for(let button of buttons){
                if(button.reset){
                    button.reset();
                }
            }
        }
    },
    computed: {
        show_next_button: function() {
            return this.current_question.correct != null;
        },
        show_finish_button: function() {
            if(app == null){
                return false;
            }

            if(this.current_question.correct == null){
                // Question not yet answered
                return false;
            }

            return this.question_number == this.questions.length;
        }
    },
    created: function(){
        // Return to menu if questions aren't loaded
        if(this.current_question == null){
            router.push("/")
        }
    }

};

// View for the final results page
const results = {
    props: ["questions"],
    template: `
        <div id="results">
            <p>{{results.correct_num}} / {{results.questions_num}}</p>

            <h2>Incorrect Questions</h2>
            <div v-for="question in questions">
                <result v-bind:question="question"></result>
            </div>
            <router-link to="/quiz" tag="button">Back</router-link>
        </div>`,
    computed:{
        results: function () {
            let questions_num = this.questions.length;
            let correct_num = 0;

            for(let question of this.questions){
                if(question.correct == true){
                    correct_num += 1;
                }
            }

            return {
                questions_num,
                correct_num
            }
        }
    }
}

// View for individual question result on results route
Vue.component("result", {
    props: ["question"],
    template: `
        <div v-if="question.correct == false">
            <h3>{{question.text}}</h3>
            <p> Correct answer: {{correct_answer}}</p>
            <p>{{question.explanation}}</p>
        </div>
        `,
    computed:{
        correct_answer: function(){
            for(answer of this.question.answers){
                if(answer.correct){
                    return answer.value;
                }
            }
            return "There's no correct answer (this may be a problem)"
        }
    }
});

// View for a single answer within the quiz
Vue.component("answer", {
    props: ["answer", "question"],
    template: `<button v-on:click='on_click' v-on:next_question="reset" v-bind:class="{clicked: clicked}" class='answer'>
                    {{answer.value}}
                    <span class="float-right green" v-if="answer.correct && display">✔</span>
                    <span class="float-right red" v-if="!answer.correct && display">✖</span>
               </button>`,
    methods: {
        on_click: function(event){
            // check if question has already been answered
            if (this.question.correct != null){
                return
            }
            this.clicked = true;
            console.log(this.answer.correct, this.question.id);
            app.questions[app.question_number].correct = this.answer.correct;
            post_result(this.question.id, this.answer.correct);
        },
        reset: function(){
            this.clicked = false;
            console.log("reset");
        }
    },
    computed: {
        display: function(){
            return this.question.correct != null;
        }
    },
    data: function(){
        return {
            clicked: false
        }
    }
});

// View for the score along the bottom
Vue.component("score", {
    props: ["questions"],
    template: `<ul id="score">
                    <li v-for="question in questions">
                       <point v-bind:question="question"></point>
                    </li>
               </ul>`,
})

// View for a single point in the score
Vue.component("point", {
    props: ["question"],
    template: `<span title="question.id" class="point correct" v-if="question.correct">✔</span>
               <span title="question.id" class="point incorrect" v-else-if="question.correct == false">✖</span>
               <span class="point" v-else>●</span>`
})

Vue.component('flag_question', {
    props: ["question"],
    data: function() {
        return {
            reason: "other",
            message: "",
        }
    },
    template: ` <transition name="modal">
                    <div class="modal-mask">
                        <div class="modal-wrapper">
                            <div class="modal-container">

                                <div class="modal-header">
                                    <slot name="header">
                                        <h2>Flagging: {{question.text}} (#{{question.id}})</h2>
                                    </slot>
                                </div>

                                <div class="modal-body">
                                    <slot name="body">
                                        <select v-model="reason">
                                            <option value="incorrect">Incorrect Answer</option>
                                            <option value="misleading_question">Misleading Question</option>
                                            <option value="other">Other</option>
                                        </select>

                                        <textarea placeholder="What did we get wrong?" rows="4" cols="80" v-model="message"></textarea>
                                    </slot>
                                </div>

                                <div class="modal-footer">
                                    <slot name="footer">
                                        <button class="modal-default-button" @click="$emit('close')">
                                            Cancel
                                        </button>
                                        <button class="modal-default-button" @click="send()">
                                            Send Report
                                        </button>
                                    </slot>
                                </div>
                             </div>
                        </div>
                    </div>
               </transition>`,
    methods:{
        send: function(){
            console.log("Reporting for", this.reason, ":", this.message);

            $.post({
                url: "/api/flag/" + this.question.id,
                data: JSON.stringify({
                    reason: this.reason,
                    message: this.message
                }),
                contentType: "application/json",
                dataType: "json",
                success: function(){
                    console.log("Success");
                    this.$emit("close");
                }
            })
        }
    }
})