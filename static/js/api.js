function request_and_process_questions(){
    request_questions(process_questions);
}

function request_questions(success){
    console.log("loading questions");
    jQuery.get({
        url: "/api/questions",
        success
    });
}

/// Takes the questions from the server, loads them into the app
function process_questions(data, status, jqXHR){
    console.log(data);
    data = data["Ok"].map(q => {q.correct = null; return q});
    app.questions = data
    console.log(data);
    app.current_question = app.questions[app.question_number];
    app.loaded = true;
}

function post_result(id, result){
    url = "/api/question/" + id;
    if (result){
        url += "/correct"
    }else {
        url += "/incorrect"
    }

    jQuery.post({
        url: url,

    })
}