var app = null;

const router = new VueRouter({
    routes: [
        { path: '/', component: main },
        { path: '/quiz', component: quiz},
        { path: '/load/:type', component: load, props: true},
        { path: '/results', component: results},
    ]
})

function change_route(new_route){
    window.location.hash = new_route;
    app.current_route = window.location.hash;
}

function create_app(){
    app = new Vue({
        el: '#app',
        router,
        data: {
            loaded: false, // if valid questions are loaded
            question_number: 0,
            current_route: window.location.hash,
            current_question: null,
            questions: null,
            stats: null,
            show_flag: false
        },
    })
}

function next_question() {
    console.log("next_question");
    app.question_number += 1;

    if(app.question_number == app.questions.length ){
        console.log("Used all questions");
        router.push("/results");
        app.current_question = null;
        return;
    }

    app.current_question = app.questions[app.question_number];
}


