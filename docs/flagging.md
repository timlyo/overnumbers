A user should be able to flag a question to make an admin aware of a problem with it.

Once flagged a certain number of a times a question should not appear in a quiz. This should be greater than one to stop a single user being able flag multiple messages and disrupt operation. ip should be recorded to stop abuse and multiple flags from a single ip should be ignored.

A user should be able to flag a question for the following reasons:

- Incorrect answer
- Misleading question
- Other

To enable action upon a flagged question some data are required:

- Question ID
- Ip of flagger (to detect spam)
- Message from user