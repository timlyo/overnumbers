import glob

def task_compile_api():
    return {
        "actions": ["cd api; cargo build --release"],
        "targets": ["api/target/release/api"]
    }

def task_copy_api_service():
    return {
        "actions": ["cp api/overnumbers-api.service output/"],
        "targets": ["api/overnumbers-api.service output"]
    }

def task_copy_api():
    return {
        "actions": ["cp api/target/release/api output"],
        "file_dep": ["api/target/release/api"],
        "targets": ["output/api"]
    }

def task_minify_css():
    return {
        "actions": ["minify static/css/style.css -o output/css/style.css"],
        "file_dep": ["static/css/style.css"]
    }

def task_minify_js():
    js = glob.glob("static/js/*.js")
    for file in js:
        name = file.split("/")[-1]
        yield {
            "name": name,
            "actions": ["minify static/js/{0} -o output/js/{0}".format(name)],
            "file_dep": [file]
        }

def task_minify_html():
    html = glob.glob("static/*.html")
    for file in html:
        name = file.split("/")[-1]
        yield {
            "name": name,
            "actions": ["minify static/{0} -o output/{0}".format(name)],
            "file_dep": [file]
        }

def task_copy_caddy_file():
    return{
        "actions": ["cp ProductionCaddyFile output/Caddyfile"],
        "file_dep": ["ProductionCaddyFile"]
    }

def task_prepare_output_folder():
    return {
        "actions": ["mkdir -p output"],
    }

def task_get_database_schema():
    return{
        "actions": ["pg_dump --schema-only -U postgres -Cs overnumbers > output/db_schema"],
        "targets": ["output/db_schema"]
    }