use rocket::Outcome;
use rocket::http::Status;
use rocket::request::{self, Request, FromRequest};
use std::net::IpAddr;
use std::str::FromStr;

#[derive(Debug)]
pub struct Ip(pub IpAddr);

impl<'a, 'r> FromRequest<'a, 'r> for Ip {
    type Error = &'static str;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Ip, &'static str> {
        let ip = match request.headers().get_one("ip") {
            Some(ip) => ip,
            None => return Outcome::Failure((Status::BadRequest, "Ip not found in header")),
        };

        if ip == "localhost" {
            return Outcome::Success(Ip(IpAddr::from_str("127.0.0.1").unwrap()));
        }

        match IpAddr::from_str(ip) {
            Ok(ip) => Outcome::Success(Ip(ip)),
            Err(e) => Outcome::Failure((Status::BadRequest, "Malformed ip")),
        }
    }
}
