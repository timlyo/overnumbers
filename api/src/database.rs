use question::{Question, Flag, FlagReasons};
use postgres::{Connection, TlsMode, error, rows};
use postgres::rows::Rows;
use std::net::IpAddr;
use std::convert::From;

#[derive(Debug)]
pub enum DatabaseError {
    Connection(error::Error),
    SQL(error::Error),
    InvalidEntry(&'static str),
}

impl From<error::Error> for DatabaseError {
    fn from(error: error::Error) -> Self {
        DatabaseError::SQL(error)
    }
}


pub fn get_connection() -> Result<Connection, DatabaseError> {
    match Connection::connect("postgres://postgres@localhost/overnumbers", TlsMode::None) {
        Ok(conn) => Ok(conn),
        Err(e) => Err(DatabaseError::Connection(e)),
    }
}

pub fn get_question_by_id(id: i32) -> Result<Rows, DatabaseError> {
    let conn = get_connection()?;
    match conn.query("SELECT * FROM questions WHERE id = $1", &[&id]) {
        Ok(rows) => Ok(rows),
        Err(e) => Err(DatabaseError::SQL(e)),
    }
}

pub fn get_random_questions(limit: i64) -> Result<Rows, DatabaseError> {
    let conn = get_connection()?;
    let query = "SELECT * FROM questions \
                 ORDER BY random() \
                 LIMIT $1";

    Ok(conn.query(query, &[&limit])?)
}

pub fn count_questions() -> Result<u32, DatabaseError> {
    let conn = get_connection()?;
    let query = "SELECT count(*) from questions;";

    match conn.query(query, &[]) {
        Ok(row) => Ok(row.get(0).get(0)),
        Err(e) => Err(DatabaseError::SQL(e)),
    }
}

/// Update the database as to whether the user got the question correct or not
pub fn update_question_stats(id: i32, correct: bool) -> Result<(), DatabaseError> {
    let conn = get_connection()?;

    let asked_query = "UPDATE questions \
                            SET times_asked = times_asked + 1 \
                         WHERE id = $1";

    let correct_query = "UPDATE questions \
                            SET times_correct = times_correct + 1 \
                         WHERE id = $1";

    // increment times asked
    let asked_modified = conn.execute(asked_query, &[&id])?;

    // increment times correct
    if correct {
        conn.execute(correct_query, &[&id])?;
    }

    Ok(())
}

pub fn get_flags_on_question(id: i32) -> Result<Vec<Flag>, DatabaseError> {
    let conn = get_connection()?;

    let query = "SELECT question_id, reason, message, cast(ip as text) FROM flags WHERE question_id = $1";
    let result = conn.query(query, &[&id])?;

    let flags = result.into_iter().map(|row| Flag::from_row(row)).collect();
    Ok(flags)
}

pub fn get_flags_by_ip_and_question(ip: IpAddr, question: i32) -> Result<Vec<Flag>, DatabaseError> {
    let conn = get_connection()?;

    Ok(vec![])
}

pub fn flag_question(
    id: i32,
    reason: &str,
    message: &str,
    ip: IpAddr,
) -> Result<(), DatabaseError> {
    let conn = get_connection()?;
    let query = "INSERT INTO flags(question_id, reason, message, ip) \
        VALUES ($1, $2, $3, cast($4::text as inet));";

    let ip = format!("{}", ip);

    if FlagReasons::from_str(reason).is_none() {
        return Err(DatabaseError::InvalidEntry("Flag reason is not recognised"));
    }

    let result = conn.execute(query, &[&id, &reason, &message, &ip])?;
    println!("{:?}", result);
    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;
    use question::Question;

    #[test]
    fn test_get_question_by_id() {
        let question = get_question_by_id(3);
        assert!(question.is_ok())
    }

    #[test]
    fn test_get_random_questions() {
        let result = get_random_questions(10);
        println!("{:?}", result);
    }

    #[test]
    fn test_update_question_stats_correct() {
        let id = 10;

        let before = Question::get_by_id(id).unwrap();
        update_question_stats(id, true);
        let after = Question::get_by_id(id).unwrap();

        println!("{:?}, {:?}", before, after);

        assert_eq!(before.times_asked + 1, after.times_asked);
        assert_eq!(before.times_correct + 1, after.times_correct);
    }

    #[test]
    fn test_update_question_stats_incorrect() {
        let id = 11;

        let before = Question::get_by_id(id).unwrap();
        update_question_stats(id, false);
        let after = Question::get_by_id(id).unwrap();

        println!("{:?}, {:?}", before, after);

        assert_eq!(before.times_asked + 1, after.times_asked);
        assert_eq!(before.times_correct, after.times_correct);
    }

    #[test]
    fn test_flag_question() {
        let result = flag_question(8, "Other", "Thing be wrong", "127.0.0.1".parse().unwrap());
        println!("{:?}", result);
        assert!(result.is_ok());
    }

    #[test]
    fn test_get_flags() {
        let result = get_flags_on_question(8);
        println!("{:?}", result);
        assert!(result.is_ok());
    }
}
