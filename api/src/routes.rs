use rocket;
use serde::ser::Serialize;
use rocket_contrib::Json;


use question;
use question::Question;
use database;
use request_guards::Ip;

#[derive(Serialize, Deserialize)]
pub struct DefaultInfo {
    name: String,
    version: String,
}

impl DefaultInfo {
    pub fn new() -> DefaultInfo {
        DefaultInfo {
            name: "Over Numbers".to_string(),
            version: "0.1.0".to_string(),
        }
    }
}

#[get("/")]
fn index() -> Json<DefaultInfo> {
    Json(DefaultInfo::new())
}

#[get("/questions")]
fn questions() -> Json<Result<Vec<Question>, String>> {
    match Question::get_selection(10) {
        Ok(q) => Json(Ok(q)),
        Err(e) => Json(Err(format!("{:?}", e))),
    }
}

#[get("/question/<id>")]
fn question(id: i32) -> Json<Result<Question, String>> {
    match Question::get_by_id(id) {
        Ok(q) => Json(Ok(q)),
        Err(e) => Json(Err(format!("{:?}", e))),
    }
}

/// Route for posting the response to a question
/// Used to update the databases statistics for how often a question is answered correctly
#[post("/question/<id>/correct")]
fn question_correct(id: i32) {
    println!("{:?} correct", id);
    database::update_question_stats(id, true);
}

/// Route for posting the response to a question
/// Used to update the databases statistics for how often a question is answered correctly
#[post("/question/<id>/incorrect")]
fn question_incorrect(id: i32) {
    println!("{:?} incorrect", id);
    database::update_question_stats(id, false);
}

#[derive(Deserialize)]
struct Flag {
    reason: String,
    message: String,
}

/// Route for adding a flag to the database
#[post("/flag/<id>", data = "<flag>")]
fn flag(ip: Ip, id: i32, flag: Json<Flag>) {
    database::flag_question(id, &flag.reason, &flag.message, ip.0);
}

pub fn start_api() {
    rocket::ignite()
        .mount(
            "/",
            routes![
                index,
                flag,
                questions,
                question,
                question_incorrect,
                question_correct,
            ],
        )
        .launch();
}
