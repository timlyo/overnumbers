use database;
use database::DatabaseError;
use std::error::Error;
use std::fmt;
use postgres::types;
use postgres::rows::Row;
use rand::{thread_rng, Rng};
use std::net::IpAddr;

#[derive(Serialize, Debug)]
pub struct Question {
    pub id: i32,
    pub text: String,
    pub answers: Vec<Answer>,
    pub times_asked: i32,
    pub times_correct: i32,
    pub explanation: Option<String>,
}

#[derive(Debug)]
pub struct Flag {
    pub id: i32,
    pub reason: FlagReasons,
    pub message: String,
    pub ip: IpAddr,
}

#[derive(Debug)]
pub enum QuestionErrors {
    DatabaseError(DatabaseError),
    QuestionError(QuestionError),
}

#[derive(Debug, PartialEq)]
pub enum FlagReasons {
    IncorrectAnswer,
    MisleadingQuestion,
    Other,
}

impl FlagReasons {
    pub fn from_str(string: &str) -> Option<FlagReasons> {
        match string {
            "IncorrectAnswer" => Some(FlagReasons::IncorrectAnswer),
            "MisleadingQuestion" => Some(FlagReasons::MisleadingQuestion),
            "Other" => Some(FlagReasons::Other),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct QuestionError {
    problem: String,
}

impl Error for QuestionError {
    fn description(&self) -> &str {
        &self.problem
    }
}

impl fmt::Display for QuestionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error: {}", self.problem)
    }
}

impl Question {
    pub fn from_row(row: Row) -> Question {
        let answer = row.get(4);
        let false_answers = row.get(5);

        Question {
            id: row.get(0),
            text: row.get(1),
            answers: Answer::create_answer_vec(answer, false_answers),
            times_asked: row.get(2),
            times_correct: row.get(3),
            explanation: row.get(6),
        }
    }

    pub fn get_by_id(id: i32) -> Result<Question, QuestionErrors> {
        let response = match database::get_question_by_id(id) {
            Ok(r) => r,
            Err(e) => return Err(QuestionErrors::DatabaseError(e)),
        };

        if response.len() == 0 {
            return Err(QuestionErrors::QuestionError(QuestionError {
                problem: format!("No question with id: {}", id),
            }));
        }

        Ok(Question::from_row(response.get(0)))
    }

    pub fn get_selection(amount: i64) -> Result<Vec<Question>, QuestionErrors> {
        let response = match database::get_random_questions(amount) {
            Ok(r) => r,
            Err(e) => return Err(QuestionErrors::DatabaseError(e)),
        };

        Ok(
            response
                .into_iter()
                .map(|row| Question::from_row(row))
                .collect(),
        )
    }
}

impl Flag {
    pub fn from_row(row: Row) -> Flag {
        let ip: String = row.get(3);
        let ip = ip.split("/").next().unwrap();
        let ip = ip.parse();
        let reason: String = row.get(1);
        let reason = FlagReasons::from_str(&reason).expect("Invalid Flag Reason");


        Flag {
            id: row.get(0),
            reason: reason,
            message: row.get(2),
            ip: ip.unwrap(),
        }
    }
}

impl fmt::Display for FlagReasons {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            &FlagReasons::IncorrectAnswer => "IncorrectAnswer",
            &FlagReasons::MisleadingQuestion => "MisleadingQuestion",
            &FlagReasons::Other => "Other",
        };

        write!(f, "{}", text)
    }
}

#[derive(Serialize, Debug)]
pub struct Answer {
    value: String,
    correct: bool,
}

impl Answer {
    pub fn create_answer_vec(correct_answer: String, false_answers: Vec<String>) -> Vec<Answer> {
        let mut answers = vec![];
        answers.push(Answer {
            value: correct_answer,
            correct: true,
        });

        for answer in false_answers {
            answers.push(Answer {
                value: answer,
                correct: false,
            })
        }

        thread_rng().shuffle(&mut answers);
        answers
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_by_id() {
        let r = Question::get_by_id(3);
        println!("{:?}", r);
    }

    #[test]
    fn test_flag_reasons_from_str() {
        assert_eq!(
            FlagReasons::from_str("IncorrectAnswer"),
            Some(FlagReasons::IncorrectAnswer)
        );
        assert_eq!(FlagReasons::from_str("Other"), Some(FlagReasons::Other));
        assert_eq!(
            FlagReasons::from_str("MisleadingQuestion"),
            Some(FlagReasons::MisleadingQuestion)
        );

    }
}
