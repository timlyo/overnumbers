#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

extern crate postgres;
extern crate rand;

mod routes;
mod question;
mod database;
mod request_guards;

fn main() {
    routes::start_api();
    println!("Hello, world!");
}
